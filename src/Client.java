import java.io.*;
import java.net.*;

class ReadFromServerThread implements Runnable{
	Socket socket;
	BufferedReader in;
	GuiGame gui;
	/**
	 * @param socket
	 * @param g
	 * Constructor
	 */
	public ReadFromServerThread(Socket socket, GuiGame g)
	{
		this.socket=socket;
		this.gui =g;
	}

	/**
	 * The thread listen to the server
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		try{
			boolean is_disconnect=false;
			in= new BufferedReader( new InputStreamReader(socket.getInputStream()));
			String serverString;
			while (!is_disconnect &&((serverString=in.readLine())!=null))
			{      
				gui.message_from_client(serverString);
				if (serverString.equals("disconnect"))
					is_disconnect=true;
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		System.out.println("Client: Disconnected from server");
	}

	/**
	 * The method close the bufferedReader
	 */
	public void closeReadFromServerThread() {
		if(this.in!=null) {
			try {
				this.in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

class Client implements Runnable{
	Socket socket;
	BufferedWriter out;
	ReadFromServerThread readThread;
	GuiGame gui;

	/**
	 * @param g
	 * Constructor
	 */
	public Client(GuiGame g)
	{
		gui =g;
		try{ 
			socket=new Socket("127.0.0.1",8000);
			out=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			Thread clientThread =new Thread(this, "client");
			clientThread.start();
		}
		catch(IOException e){
			System.err.println("Client: Couldn't get I/O for the connection to server.");
			System.exit(1);
		}
	}


	/**
	 * thread that listen to the server
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		try{
			readThread=new ReadFromServerThread(socket, gui);
			Thread read=new Thread(readThread,"read");
			read.start();


		}catch (Exception e){
			System.err.println("Client: Couldn't read or write");
			System.exit(1);
		}
	}


	/**
	 * The method close the client
	 */
	public void closeClient() {
		if(this.readThread!=null) {
			this.readThread.closeReadFromServerThread();
		}
		if(this.out!=null) {
			try {
				this.out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(this.socket!=null) {
			try {
				this.socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		readThread.closeReadFromServerThread();
	}

	/**
	 * @param msg
	 * The method send messages to the server
	 */
	public void send_to_server(String msg)
	{
		try {
			out.write(msg+"\n");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

