import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FinalScreen extends JPanel implements ActionListener 
{
	private JFrame f=new JFrame();
	private JButton moreG=new JButton();
	private JButton bye=new JButton();
	private JButton b=new JButton();
	private Image[]img;
	private Icon [] icon;
	private JLabel lWin=new JLabel();
	private JLabel lLose=new JLabel();
	private boolean win;
	private Client client;

	/**
	 * @param win
	 * @param client 
	 * Constructor
	 */
	public FinalScreen(boolean win,Client client)
	{
		this.win=win;
		this.client=client;
		this.setLayout(null);
		img=new Image[6];
		icon=new Icon[6];
		img[0]= this.getToolkit().getImage("Resources\\win.png");
		icon[0]=new ImageIcon(img[0]);
		img[1]= this.getToolkit().getImage("Resources\\lose.png");
		icon[1]=new ImageIcon(img[1]);
		img[2]= this.getToolkit().getImage("Resources\\newGame.png");
		icon[2]=new ImageIcon(img[2]);
		img[3]= this.getToolkit().getImage("Resources\\exitG.png");
		icon[3]=new ImageIcon(img[3]);
		img[4]= this.getToolkit().getImage("Resources\\finalSc.png");
		icon[4]=new ImageIcon(img[4]);
		lWin.setIcon(icon[0]);
		lLose.setIcon(icon[1]);
		moreG.setIcon(icon[2]);
		bye.setIcon(icon[3]);
		moreG.setBackground(Color.white);
		bye.setBackground(Color.white);
		bye.setBounds(50, 170,150,50);
		moreG.setBounds(300, 170,150,50);
		lWin.setBounds(125,-20, 250, 200);
		lLose.setBounds(125,-20, 250, 200);
		bye.addActionListener(this);
		moreG.addActionListener(this);
		lLose.setVisible(false);
		lWin.setVisible(false);
		this.add(lLose);
		this.add(lWin);
		this.add(moreG);
		this.add(bye);
		if(win)
		{
			lWin.setVisible(true);
		}
		if(!win)
		{
			lLose.setVisible(true);
		}

		f.add(this);
		f.setSize(500, 300);
		f.setResizable(false);
		f.setVisible(true);

	}


	/**
	 * @param g
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		g.drawImage(img[4],0,0,this.getWidth(),this.getHeight(),null);

	}

	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e)
	{
		b=(JButton)e.getSource();
		if(b==bye)
		{
			f.setVisible(false);
			client.send_to_server("disconnect");
			client.closeClient();

		}
		if(b==moreG)
		{
			client.send_to_server("more game");
			JOptionPane.showMessageDialog(null, "מחכה לשחקן השני ");
		}

	}

	/**
	 * @return JFrame
	 */
	public JFrame getF()
	{
		return this.f;
	}

}
