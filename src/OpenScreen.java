import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class OpenScreen extends JPanel implements ActionListener
{
	private JFrame f=new JFrame();
	private Image background;
	private Icon icon;
	private JButton begin=new JButton();
	private JButton instruction=new JButton();
	private Image[]im;
	private Icon [] iconm;
	private Gui game;
	private InstructionScreen in;

	/**
	 * Constructor
	 */
	public OpenScreen()
	{
		begin.addActionListener(this);
		instruction.addActionListener(this);
		im=new Image[2];
		iconm=new Icon[2];
		im[1]= this.getToolkit().getImage("Resources\\instructionButton.png");
		im[0]= this.getToolkit().getImage("Resources\\playButton.png");
		im[0]=im[0].getScaledInstance(160,100,Image.SCALE_DEFAULT);
		iconm[0]=new ImageIcon(im[0]);
		begin.setIcon(iconm[0]);
		im[1]=im[1].getScaledInstance(160,100,Image.SCALE_DEFAULT);
		iconm[1]=new ImageIcon(im[1]);
		instruction.setIcon(iconm[1]);
		background= this.getToolkit().getImage("Resources\\openingScreen.png");
		icon=new ImageIcon(background);
		this.setLayout(null);
		begin.setBackground(Color.gray);
		instruction.setBounds(100, 300,150,100);
		begin.setBounds(550, 300,150,100);
		this.add(begin);
		this.add(instruction);
		f.setResizable(false);
		f.add(this);
		f.setSize(800,500);
		f.setVisible(true);   
	}

	/**
	 * @param g
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		g.drawImage(background,0,0,this.getWidth(),this.getHeight(),null);
	}

	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e)
	{
		if((JButton)e.getSource()==begin)
		{
			game=new Gui(true);
			f.setVisible(false);
		}

		if((JButton)e.getSource()==instruction)
		{
			in=new InstructionScreen();
			f.setVisible(false);
		}

	}
}
