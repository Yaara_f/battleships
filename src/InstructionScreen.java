import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class InstructionScreen  extends JPanel implements ActionListener 
{
	private Image background2;
	private Icon icon2;
	private JFrame f2=new JFrame();
	private JButton exit=new JButton();
	private OpenScreen open;
	private Icon exitB;
	private Image exitButton;

	/**
	 * Constructor
	 */
	public InstructionScreen()
	{	
		background2= this.getToolkit().getImage("Resources\\instructionPanel.png");
		icon2=new ImageIcon(background2);
		exitButton= this.getToolkit().getImage("Resources\\exit.png");
		exitButton=exitButton.getScaledInstance(20,20,Image.SCALE_DEFAULT);
		exit.setBackground(Color.black);
		exitB=new ImageIcon(exitButton);
		exit.setIcon(exitB);
		exit.addActionListener(this);
		this.setLayout(null);
		exit.setBounds(750, 0,25,25);
		this.add(exit);
		f2.add(this);
		f2.setSize(800,500);
		f2.setResizable(false);
		f2.setVisible(true);
	}

	/**
	 * @param g
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		g.drawImage(background2,0,0,this.getWidth(),this.getHeight(),null);
	}

	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e)
	{
		if((JButton)e.getSource()==exit)
		{
			open=new OpenScreen();
			f2.setVisible(false);
		}
	}

}
