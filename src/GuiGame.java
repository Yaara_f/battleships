import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GuiGame implements ActionListener {
    private JFrame fGame = new JFrame();
    private JPanel pUp = new JPanel();
    private JPanel pShip = new JPanel();
    private JPanel pGMy = new JPanel();
    private JPanel pGEn = new JPanel();
    private JPanel pCenter = new JPanel();
    private JButton[][] myBoard;
    private JButton[][] enemyBoard;
    private JLabel lMy = new JLabel();
    private JLabel lEnemy = new JLabel();
    private JLabel turn = new JLabel();
    private JLabel noturn = new JLabel();
    private JLabel toDrown = new JLabel();
    private Image img[];
    private Icon icon[];
    private Board Mybrd = new Board();
    private Board enemybrd = new Board();
    private JButton b = new JButton();
    private Client client;
    private String msg = "";
    private int stayedForWin = 5;
    private boolean getEnemy = false;
    private boolean turnf;
    private FinalScreen finalS;
    private boolean fWin = false;
    private String[] shipsAr = new String[5];
    private String[] a;
    private boolean firstGame;
    private Gui gui;
    private boolean win = false;


    /**
     * @param gui
     * @param firstG Constructor
     */
    public GuiGame(Gui gui, boolean firstG) {
        this.gui = gui;
        this.firstGame = firstG;
        turn.setText("תורך");
        turn.setBackground(Color.blue);
        turn.setSize(100, 100);
        noturn.setText("התור של היריב ");
        toDrown.setText("<html><br>" + "נותרו לך להטביע עוד  " + stayedForWin + "  סירות כדי לנצח" + "</html>");
        toDrown.setFont(new Font("arial", Font.BOLD, 16));
        toDrown.setForeground(Color.cyan);
        img = new Image[5];
        icon = new Icon[5];
        pGMy.setLayout(new GridLayout(10, 10));
        pGEn.setLayout(new GridLayout(10, 10));
        pUp.setLayout(new BorderLayout());
        myBoard = new JButton[10][10];
        turn.setFont(new Font("arial", Font.BOLD, 16));
        turn.setForeground(Color.cyan);
        noturn.setFont(new Font("arial", Font.BOLD, 14));
        noturn.setForeground(Color.cyan);
        for (int i = 0; i < myBoard.length; i++)
            for (int j = 0; j < myBoard.length; j++)//do the button in the array
            {
                myBoard[i][j] = new JButton();
                myBoard[i][j].setPreferredSize(new Dimension(20, 20));
                if (Mybrd.get_boardMat()[i][j].is_ship()) {
                    myBoard[i][j].setBackground(Color.red);
                } else
                    myBoard[i][j].setBackground(Color.blue);
                myBoard[i][j].setName(i + "," + j);
                pGMy.add(myBoard[i][j]);

            }

        enemyBoard = new JButton[10][10];
        for (int i = 0; i < enemyBoard.length; i++)
            for (int j = 0; j < enemyBoard.length; j++)//do the button in the array
            {
                enemyBoard[i][j] = new JButton();
                enemyBoard[i][j].addActionListener(this);
                enemyBoard[i][j].setPreferredSize(new Dimension(50, 50));
                enemyBoard[i][j].setBackground(Color.blue);
                enemyBoard[i][j].setName(i + "," + j);
                pGEn.add(enemyBoard[i][j]);
            }


        img[0] = pUp.getToolkit().getImage("Resources\\my.png");
        img[0] = img[0].getScaledInstance(150, 100, Image.SCALE_DEFAULT);
        icon[0] = new ImageIcon(img[0]);
        img[1] = pUp.getToolkit().getImage("Resources\\enemy.png");
        img[1] = img[1].getScaledInstance(150, 100, Image.SCALE_DEFAULT);
        icon[1] = new ImageIcon(img[1]);
        lMy.setIcon(icon[0]);
        lEnemy.setIcon(icon[1]);

        img[2] = pUp.getToolkit().getImage("Resources\\no.png");
        img[2] = img[2].getScaledInstance(50, 50, Image.SCALE_DEFAULT);
        icon[2] = new ImageIcon(img[2]);

        img[3] = pUp.getToolkit().getImage("Resources\\explo.png");
        img[3] = img[3].getScaledInstance(50, 50, Image.SCALE_DEFAULT);
        icon[3] = new ImageIcon(img[3]);

        img[4] = pUp.getToolkit().getImage("Resources\\boom.png");
        img[4] = img[4].getScaledInstance(20, 20, Image.SCALE_DEFAULT);
        icon[4] = new ImageIcon(img[4]);


        turn.setVisible(false);
        noturn.setVisible(false);
        pCenter.add(turn);
        pCenter.add(noturn);
        pShip.setBackground(Color.black);
        pShip.add(toDrown);
        pUp.add(lMy, BorderLayout.EAST);
        pUp.add(pShip, BorderLayout.CENTER);
        pUp.add(lEnemy, BorderLayout.WEST);
        pCenter.setBackground(Color.black);
        pUp.setBackground(Color.black);
        fGame.setLayout(new BorderLayout());
        fGame.add(pUp, BorderLayout.NORTH);
        fGame.add(pGEn, BorderLayout.WEST);
        fGame.add(pCenter, BorderLayout.CENTER);
        fGame.add(pGMy, BorderLayout.EAST);
        fGame.setSize(800, 500);
        fGame.setResizable(false);
        fGame.setVisible(true);

        if (!getEnemy) {
            toDrown.setForeground(Color.red);
            toDrown.setFont(new Font("arial", Font.BOLD, 26));
            toDrown.setText("<html><br>" + "מחכה לשחקן השני" + "</html>");
        }

        String msg = null;
        for (int i = 0; i < Mybrd.get_boardMat().length; i++) {
            for (int j = 0; j < Mybrd.get_boardMat().length; j++) {
                msg += Mybrd.get_cell_num(new Point(i, j)) + ",";
            }
        }
        if (firstGame) {
            client = new Client(this);
        }

        if (!getEnemy && firstGame) {
            toDrown.setText("<html><br>" + "מחכה לשחקן השני" + "</html>");
            toDrown.setFont(new Font("arial", Font.BOLD, 26));
            toDrown.setForeground(Color.red);
        }

    }


    /**
     * @param e
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if (!turnf && getEnemy) {
            JOptionPane.showMessageDialog(null, "זהו לא תורך");
        }


        if (turnf) {
            if (getEnemy) {
                toDrown.setText("<html><br>" + "נותרו לך להטביע עוד  " + stayedForWin + "  סירות כדי לנצח" + "</html>");
                toDrown.setFont(new Font("arial", Font.BOLD, 16));
                toDrown.setForeground(Color.cyan);
                b = (JButton) e.getSource();
                for (int i = 0; i < enemyBoard.length; i++)
                    for (int j = 0; j < enemyBoard.length; j++) {
                        if (enemyBoard[i][j] == b && !enemybrd.get_boardMat()[i][j].getClicked()) {
                            enemybrd.get_boardMat()[i][j].setClick(true);
                            String msg = i + "," + j;
                            msg = play(i, j);
                            client.send_to_server(msg);
                            if (!fWin) {
                                msg = "next turn";
                                client.send_to_server(msg);
                                turn.setVisible(false);
                                noturn.setVisible(true);
                                turnf = false;
                            }

                        }
                    }
            }

        }

    }


    /**
     * The method send to the server message that contains all the places around the ships.
     */
    public void shipMessage() {
        String msg = "";
        for (int i = 0; i < Mybrd.getShips().length; i++) {
            for (int j = 0; j < Mybrd.getShips()[i].getAroundPlaces().size(); j++) {
                msg += Mybrd.getShips()[i].getAroundPlaces().get(j).x + "," + Mybrd.getShips()[i].getAroundPlaces().get(j).y + "_";
            }

            msg += "@";
        }
        client.send_to_server("ship" + msg);
    }


    /**
     * @param i
     * @param j
     * @return String
     * The method check if there was hit or not, and return the appropriate message.
     */
    public String play(int i, int j) {
        int shipNum = enemybrd.get_boardMat()[i][j].getIndex_ship();
        if (enemybrd.get_boardMat()[i][j].is_ship()) {
            enemybrd.getShips()[shipNum].add_hit();
            if (enemybrd.check_if_drown(shipNum)) {
                b.setIcon(icon[3]);
                draw_around_X(shipNum);
                stayedForWin--;
                if (stayedForWin == 0) {
                    fGame.setVisible(false);
                    win = true;
                    fWin = true;
                    finalS = new FinalScreen(win, client);

                    return "win";
                }
                if (stayedForWin != 0) {
                    JOptionPane.showMessageDialog(null, "הטבעת את הסירה נותר לך להטביע עוד " + stayedForWin + " כדי לנצח");
                }
                toDrown.setText("<html><br>" + "נותרו לך להטביע עוד  " + stayedForWin + "  סירות כדי לנצח" + "</html>");
                msg = "" + "drown" + "," + i + "," + j;
                return msg;

            } else if (!enemybrd.check_if_drown(shipNum)) {
                JOptionPane.showMessageDialog(null, "פגעת בחלק מן הסירה של היריב שלך  ");

                b.setIcon(icon[3]);
                msg = "" + "hit" + "," + i + "," + j;
                return msg;
            }
        }

        if (!enemybrd.get_boardMat()[i][j].is_ship()) {
            JOptionPane.showMessageDialog(null, "לא פגעת בסירה ");
            b.setIcon(icon[2]);
            msg = "" + "no" + "," + i + "," + j;
            return msg;
        }
        return "n";
    }

    /**
     * @param brd
     */
    public void set_board(Board brd) {
        Mybrd = brd;
    }


    /**
     * The method send to the enemy the board
     */
    public void send_board_to_enemy() {
        if (!firstGame) {
            msg = "";
            //msg=new String();
        }
        for (int i = 0; i < Mybrd.get_boardMat().length; i++) {
            for (int j = 0; j < Mybrd.get_boardMat().length; j++) {
                msg += Mybrd.get_boardMat()[i][j].getIndex_ship() + ",";
                if (Mybrd.get_boardMat()[i][j].is_ship()) {
                    myBoard[i][j].setBackground(Color.red);
                }
            }
        }

        client.send_to_server(msg);
    }


    /**
     * @param message The method get message from client and act according to the message.
     */
    public void message_from_client(String message) {
        if (message.equals("win")) {
            fGame.setVisible(false);
            win = false;
            finalS = new FinalScreen(win, client);
            fWin = true;

        }

        if (message.equals("more game")) {
            int result = JOptionPane.showConfirmDialog(null, "השחקן השני רוצה להמשיך לעוד משחק,רוצה להצטרף? ", null, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                client.send_to_server("want more");
                firstGame = false;
                getEnemy = false;
                gui.resetGame();
                finalS.getF().setVisible(false);

            }
            if (result == JOptionPane.NO_OPTION) {
                client.send_to_server("disconnect");
                finalS.getF().setVisible(false);
                client.closeClient();
            }
        }


        if (message.equals("want more")) {
            getEnemy = false;
            firstGame = false;
            gui.resetGame();
            finalS.getF().setVisible(false);

        }

        if (message.equals("disconnect")) {

            JOptionPane.showMessageDialog(null, "השחקן השני התנתק,תודה ששיחקת! ");
            finalS.getF().setVisible(false);
            client.closeClient();
        }


        if (message.equals("get")) {
            getEnemy = true;
            toDrown.setText("<html><br>" + "נותרו לך להטביע עוד  " + stayedForWin + "  סירות כדי לנצח" + "</html>");
            toDrown.setFont(new Font("arial", Font.BOLD, 16));
            toDrown.setForeground(Color.cyan);
            send_board_to_enemy();
            shipMessage();
            if (!firstGame) {
                which_user_start();
            }
        } else {
            if (!fWin) {
                if (message.substring(0, 4).equals("ship")) {
                    String msgt = message.substring(4, message.length());
                    shipsAr = msgt.split("@");

                } else {
                    String[] m = message.split(",");
                    if (message.equals("you first")) {
                        JOptionPane.showMessageDialog(null, "אתה מתחיל ");
                        turnf = true;
                        turn.setVisible(true);
                        noturn.setVisible(false);

                    }
                    if (message.equals("you second")) {
                        JOptionPane.showMessageDialog(null, "היריב מתחיל ");
                        turnf = false;
                        noturn.setVisible(true);
                        turn.setVisible(false);
                    }

                    if (message.equals("next turn")) {
                        turnf = true;
                        turn.setVisible(true);
                        noturn.setVisible(false);
                    }

                    if (m.length == 3) {


                        if (m[0].equals("hit")) {
                            JOptionPane.showMessageDialog(null, "היריב פגע לך בחלק מן הצוללת ");
                            int i = Integer.parseInt(m[1]);
                            int j = Integer.parseInt(m[2]);
                            myBoard[i][j].setIcon(icon[4]);
                        }

                        if (m[0].equals("drown")) {
                            JOptionPane.showMessageDialog(null, "היריב הטביע לך את הצוללת ");
                            int i = Integer.parseInt(m[1]);
                            int j = Integer.parseInt(m[2]);
                            myBoard[i][j].setIcon(icon[4]);

                        }
                        if (m[0].equals("no")) {
                            JOptionPane.showMessageDialog(null, "היריב החטיא ");

                        }

                    }
                    if (m.length > 3 && !message.substring(0, 4).equals("ship")) {
                        int counter = 0;
                        for (int i = 0; i < enemybrd.get_boardMat().length; i++) {
                            for (int j = 0; j < enemybrd.get_boardMat().length; j++) {
                                int temp=Integer.parseInt(m[counter]);
                                if(temp==-1) {
                                    enemybrd.get_boardMat()[i][j] = (new Cell(false));
                                }else {
                                    enemybrd.get_boardMat()[i][j] = (new Cell(temp, false));
                                }
                                counter++;

                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * @param shipIndex
     * The method draw X around the given ship
     */
    public void draw_around_X(int shipIndex) {
        a = shipsAr[shipIndex].split("_");

        for (int i = 0; i < a.length; i++) {
            int x = Integer.parseInt(a[i].charAt(0) + "");
            int y = Integer.parseInt(a[i].charAt(2) + "");
            enemybrd.get_boardMat()[x][y].setClick(true);
            enemyBoard[x][y].setIcon(icon[2]);
        }
    }


    /**
     * The method reset the gui for the next game
     */
    public void create_new_game() {
        toDrown.setForeground(Color.red);
        toDrown.setFont(new Font("arial", Font.BOLD, 26));
        toDrown.setText("<html><br>" + "מחכה לשחקן השני" + "</html>");
        for (int i = 0; i < enemyBoard.length; i++) {
            for (int j = 0; j < enemyBoard.length; j++) {
                myBoard[i][j].setBackground(Color.blue);
                enemyBoard[i][j].setIcon(null);
                myBoard[i][j].setIcon(null);
            }
        }
        enemybrd.buildShip();
        getEnemy = false;
        fWin = false;
        stayedForWin = 5;
        client.send_to_server("get");
        fGame.setVisible(true);

    }

    /**
     * The method check which player start- the player who win the last game start.
     */
    public void which_user_start() {
        if (win) {
            JOptionPane.showMessageDialog(null, "אתה מתחיל ");
            turnf = true;
            turn.setVisible(true);
            noturn.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(null, "היריב מתחיל ");
            turnf = false;
            noturn.setVisible(true);
            turn.setVisible(false);
        }

    }

}






