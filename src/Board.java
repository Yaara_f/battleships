import java.awt.Point;

public class Board
{
	private Cell [][] boardMat;
	private Point p;
	private int size=5;
	private Ship[]ships=new Ship[size];
	private boolean ans =false;//if there is exception from Board

	/**
	 * Constructor of the board
	 */
	public Board()
	{
		p=new Point();
		boardMat=new Cell[10][10];
		for(int i=0;i<boardMat.length;i++)
			for(int j=0;j<boardMat.length;j++)
			{
				boardMat[i][j]=new Cell(false);

			}
		buildShip();
	}


	/**
	 * The method initialize the ships array
	 */
	public void buildShip()
	{

		ships[0]=new Ship(2);
		ships[1]=new Ship(3);
		ships[2]=new Ship(3);
		ships[3]=new Ship(4);
		ships[4]=new Ship(5);
	}

	/**
	 * @return Cell[] boardMat
	 */
	public Cell [][] get_boardMat()
	{
		return boardMat;
	}

	/**
	 * @param  p
	 * @return int - the number in the cell
	 */
	public int get_cell_num(Point p)
	{
		return boardMat[p.x][p.y].getIndex_ship();
	}

	/**
	 * @param point
	 * @param size
	 * @param d
	 * The method check if it possible to place the ship at a given point
	 * @return boolean
	 */
	public boolean checkIfAvailable(Point point,int size,String d)
	{
		if(size==0) {
			return false;
		}
		if(d.equals("w"))
		{
			for(int i=point.y;i<point.y+size;i++)
			{
				if (if_has_ship(new Point(point.x,i))|| check_around(new Point(point.x,i)))
					return false;
			}
		}

		if(d.equals("l"))
		{
			for(int i=point.x;i<point.x+size;i++)
			{;
				if (if_has_ship(new Point(i,point.y))|| check_around(new Point(i,point.y)))
					return false;
			}
		}
		return true;
	}

	/**
	 * @param point
	 * The method check if there is a ship in the given point
	 * @return boolean
	 */
	public boolean if_has_ship(Point point )
	{
		return boardMat[point.x][point.y].is_ship();
	}

	/**
	 * @param point
	 * The method check if around the given point there is a ship
	 * @return boolean
	 */
	public boolean check_around(Point point )
	{
		return boardMat[point.x][point.y].isAround();
	}

	/**
	 * @param point
	 * @param direction
	 * @param ship
	 * The method placed ship in the given point
	 */
	public void placed (Point point,String direction ,int ship)
	{
		ships[ship].setDirection(direction);
		ships[ship].setIfPlaced(true);
		int j=point.x;
		if(direction.equals("w"))
		{
			for(int i=point.y;i<point.y+ships[ship].getSizeShip();i++)
			{
				boardMat[j][i].setIndex_ship(ship);
				ships[ship].setPlaces(new Point(j,i));
				placed_around(j,i,direction,ship);
				if(i==point.y&&i>0)
				{
					boardMat[j][i-1].set_around();
					boardMat[j][i-1].add_counterAroundShip();
					ships[ship].setAroundPlaces(new Point(j,i-1));
					placed_around(j,i-1,direction,ship);
				}
				else if(i==point.y+ships[ship].getSizeShip()-1&&i<9)
				{

					boardMat[j][i+1].set_around();
					boardMat[j][i+1].add_counterAroundShip();
					ships[ship].setAroundPlaces(new Point(j,i+1));
					placed_around(j,i+1,direction,ship);
				}
			}
		}
		if (direction.equals("l"))
		{
			j=point.y;
			for(int i=point.x;i<point.x+ships[ship].getSizeShip();i++)
			{
				ships[ship].setPlaces(new Point(j,i));
				boardMat[i][j].setIndex_ship(ship);
				placed_around(j,i,direction,ship);
				if(i==point.x&&i>0)
				{
					boardMat[i-1][j].set_around();
					boardMat[i-1][j].add_counterAroundShip();
					ships[ship].setAroundPlaces(new Point(i-1,j));
					placed_around(j,i-1,direction,ship);
				}
				else if(i==point.x+ships[ship].getSizeShip()-1&&i<9)
				{
					boardMat[i+1][j].set_around();
					boardMat[i+1][j].add_counterAroundShip();
					ships[ship].setAroundPlaces(new Point(i+1,j));
					placed_around(j,i+1,direction,ship);
				}
			}
		}
	}

	/**
	 * @param x
	 * @param y
	 * @param direction
	 * @param ship
	 *  The method save the places around the ship
	 */
	public void placed_around(int x, int y, String direction, int ship)
	{

		if(direction.equals("w"))
		{
			if(x<9)
			{
				boardMat[x+1][y].set_around();
				boardMat[x+1][y].add_counterAroundShip();
				ships[ship].setAroundPlaces(new Point(x+1,y));
			}
			if(x>0)
			{
				boardMat[x-1][y].set_around();
				boardMat[x-1][y].add_counterAroundShip();
				ships[ship].setAroundPlaces(new Point(x-1,y));
			}
		}
		if(direction.equals("l"))
		{
			if(x<9)
			{
				boardMat[y][x+1].set_around();
				boardMat[y][x+1].add_counterAroundShip();
				ships[ship].setAroundPlaces(new Point(y,x+1));
			}

			if(x>0)
			{
				boardMat[y][x-1].set_around();
				boardMat[y][x-1].add_counterAroundShip();
				ships[ship].setAroundPlaces(new Point(y,x-1));
			}
		}
	}

	/**
	 * @param point
	 * @param direction
	 * @param size
	 * @return boolean
	 * The method check if the ship exceed the limits of the board.
	 */
	public boolean is_exception_board(Point point, String direction, int size)
	{
		ans =false;
		if(direction.equals("w"))
		{
			for (int i=point.y; i <=boardMat.length&&i<=point.y+size-1; i++)
			{
				if (i+1>boardMat.length)
					ans =true;
			}

		}
		else if(direction.equals("l"))
		{
			for (int i=point.x;i<=boardMat.length&&i<=point.x+size-1; i++)
			{
				if (i+1>boardMat.length)
					ans =true;
			}
		}
		return ans;
	}


	/**
	 * @param ship
	 * @param direction
	 * The method remove ship from the board
	 */
	public void remove_ship(int ship, String direction)
	{
		Ship ship_to_remove=ships[ship];
		for(int i=0;i<ship_to_remove.getPlaces().size();i++)
		{
			if(direction.equals("w"))
				boardMat[ship_to_remove.getPlaces().get(i).x][ship_to_remove.getPlaces().get(i).y].set_empty();
			if(direction.equals("l"))
				boardMat[ship_to_remove.getPlaces().get(i).y][ship_to_remove.getPlaces().get(i).x].set_empty();
		}
		for(int i=0;i<ship_to_remove.getAroundPlaces().size();i++)
		{
			if(boardMat[ship_to_remove.getAroundPlaces().get(i).x][ship_to_remove.getAroundPlaces().get(i).y].getCounterAroundShip()==1)
			{
				boardMat[ship_to_remove.getAroundPlaces().get(i).x][ship_to_remove.getAroundPlaces().get(i).y].set_empty();
				boardMat[ship_to_remove.getAroundPlaces().get(i).x][ship_to_remove.getAroundPlaces().get(i).y].less_counterAroundShip();
			}
			else
			{
				boardMat[ship_to_remove.getAroundPlaces().get(i).x][ship_to_remove.getAroundPlaces().get(i).y].less_counterAroundShip();
			}
		}
		ship_to_remove.delete_around_ship();
		ship_to_remove.delete_ship_from_board();
	}

	/**
	 * @param s
	 * @return Point c
	 * The method convert string to point,
	 */
	public static Point from_string_to_point(String s)
	{
		Point p=new Point(Integer.parseInt(s.charAt(0)+""),Integer.parseInt(s.charAt(2)+""));
		return p;

	}

	/**
	 * @param ship_index
	 * @return boolean
	 * The method check if ship drown.
	 */
	public boolean check_if_drown(int ship_index)
	{
		if(ships[ship_index].getCounter_hit()==ships[ship_index].getSizeShip())
			return true;
		return false;
	}

	/**
	 * @param s
	 */
	public void setShips(Ship [] s)
	{
		ships =s;
	}
	/**
	 * @return Ship [] ships
	 */
	public Ship [] getShips()
	{
		return ships;
	}
}


