
public class Cell
{
    private int index_ship;
    private boolean isAround;
    private boolean isEmpty;
    private boolean click=false;
    private int counterAroundShip =0;

    /**
     * @param index_ship
     * @param click
     * Constructor
     */
    public Cell (int index_ship, boolean click)
    {
        this.index_ship = index_ship;
        this.click=click;
        this.isAround=false;
        this.isEmpty=false;
    }

    public Cell (boolean click)
    {
        this.index_ship =-1;
        this.click=click;
        this.isAround=false;
        this.isEmpty=true;
    }


    public boolean isAround() {
        return isAround;
    }

    public boolean isEmpty() {
        return isEmpty;
    }


    /**
     * @return boolean
     */
    public boolean getClicked()
    {
        return this.click;
    }

    /**
     * @param click
     */
    public void setClick(boolean click)
    {
        this.click = click;
    }

    /**
     * @return int
     */
    public int getIndex_ship()
    {
        return this.index_ship;
    }

    /**
     * @param index_ship
     */
    public void setIndex_ship(int index_ship)
    {
        this.index_ship = index_ship;
        this.isEmpty=false;
        this.isAround=false;
    }

    /**
     * @param counterAroundShip
     */
    public void setCounterAroundShip(int counterAroundShip)
    {
        this.counterAroundShip = counterAroundShip;
    }

    /**
     * @return int
     */
    public int getCounterAroundShip()
    {
        return this.counterAroundShip;
    }

    public void add_counterAroundShip()
    {
        counterAroundShip++;
    }

    public void less_counterAroundShip()
    {
        counterAroundShip--;
    }
    public boolean is_ship(){
        if(this.isEmpty||this.isAround)
            return false;
        return true;
    }

    public void set_around(){
        this.isAround=true;
        this.isEmpty=false;
        this.index_ship=-1;
    }

    public void set_empty(){
        this.isEmpty=true;
        this.isAround=false;
        this.index_ship=-1;
    }

    public void reset_cell(){
        this.isAround=false;
        this.isEmpty=true;
        this.click=false;
        this.index_ship=-1;
        this.counterAroundShip=0;
    }

}


