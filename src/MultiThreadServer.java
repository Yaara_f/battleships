import java.io.*;
import java.net.*;

class ServerThread implements Runnable {


	private Socket socket;
	private BufferedWriter out;
	private BufferedReader in;
	private ServerThread enemy;
	private boolean flag=false;
	MultiThreadServer mServer;


	public ServerThread(Socket socket,MultiThreadServer ms){
		this.socket=socket;
		mServer=ms;
	}

	/**
	 * Thread that Listens to clients and waits for messages from them.
	 * When a client sends a message, the function sends this message to the other client that connected to the server.
	 * @see java.lang.Runnable#run()
	 */
	public void run(){

		try{
			System.out.println("run");
			out =new BufferedWriter( new OutputStreamWriter(socket.getOutputStream()));
			in = new BufferedReader(new InputStreamReader (socket.getInputStream()));
			flag=true;
			String  line;

			while (( line=in.readLine())!=null)
			{
				if (line.equals("get"))
					mServer.notify_start();
				else
					enemy.sendToClient(line);
			}

		} catch (IOException e){
			e.printStackTrace();
		}
	}   

	/**
	 * @param s
	 * @param n
	 * @throws IOException
	 */
	public void set_enemy(ServerThread s, boolean n) throws IOException
	{
		this.enemy =s;
	}

	/**
     * The method send massages to the client
	 * @param msg
	 * @throws IOException
	 */
	public void sendToClient(String msg) throws IOException
	{
		out.write(msg+"\n");
		out.flush();
	}

	/**
	 * @return boolean
	 */
	public boolean getflag()
	{
		return this.flag;
	}

}

public class MultiThreadServer{
	ServerThread st1;
	ServerThread st2;
	int countGet=0;
	/**
     * The method waiting for both players to reach the part of the connection and then create two players.
	 */
	public void runGameServer()
	{
		ServerSocket serverSocket;
		try{
			serverSocket = new ServerSocket(8000);
			System.out.println("I am online");

			Socket sock1 = serverSocket.accept();

			System.out.println("Server: Connection accepted");
			st1=new ServerThread(sock1,this);//create first player
			Thread t1=new Thread(st1); 
			t1.start();

			Socket sock2 = serverSocket.accept();
			System.out.println("Server: Connection accepted");

			st2=new ServerThread(sock2,this);//create second player
			Thread t2=new Thread(st2); 
			t2.start();

			while(!st1.getflag()||!st2.getflag())				
			{

			}
			st1.sendToClient("you first");
			st2.sendToClient("you second");	


			st1.set_enemy(st2,true);
			st2.set_enemy(st1,false);
			countGet=1;
			notify_start();
		} catch (IOException e){
			System.out.println("Server: Could not connect.");
			System.exit(1);        
		}


	}

	/**
     * The method notifies players that the game is starting
	 * @throws IOException
	 */
	public void notify_start() throws IOException
	{
		countGet++;
		if (countGet==2)
		{
			st1.sendToClient("get");
			st2.sendToClient("get");
			countGet=0;
		}


	}


	public static void main(String [] args){
		MultiThreadServer server=new MultiThreadServer();
		server.runGameServer();
	}
}