import java.awt.Point;
import java.util.*;

public class Ship
{
	private int sizeShip;
	private boolean drown;
	private String direction;//Is the boat portrait or landscape?
	private ArrayList<Point> places=new ArrayList<Point>();//Where is the ship on the board
	private ArrayList<Point> aroundPlaces=new ArrayList<Point>();//places around the ship
	private boolean ifPlaced;
	private int counter_hit;

	/**
	 * @param size
	 * Constructor
	 */
	public Ship(int size)
	{
		this.sizeShip=size;
		this.counter_hit=0;
	}

	/**
	 * The method Add hits to the ship
	 */
	public void add_hit()
	{
		counter_hit++;
	}



	/**
	 * The method remove the places of the ship.
	 */
	public void delete_ship_from_board()
	{
		places.removeAll( places);
	}

	/**
	 * The method remove all the places around the ship.
	 */
	public void delete_around_ship()
	{
		aroundPlaces.removeAll(aroundPlaces);
	}


	/**
	 * @param p
	 */
	public void setPlaces( Point p )
	{
		places.add(p);
	} 


	/**
	 * @param p
	 */
	public void setAroundPlaces( Point p )
	{
		aroundPlaces.add(p);
	}



	/**
	 * @param di
	 */
	public void setDirection( String di )
	{
		direction =di;
	}


	/**
	 * @param i
	 */
	public void setIfPlaced( boolean i)
	{
		ifPlaced =i;
	} 


	/**
	 * @return int
	 */
	public int getSizeShip()
	{
		return sizeShip;
	} 

	/**
	 * @return int
	 */
	public int getCounter_hit()
	{
		return counter_hit;
	}

	/**
	 * @return String
	 */
	public String getDirection()
	{
		return direction;
	}

	/**
	 * @return ArrayList<Point>
	 */
	public ArrayList<Point> getPlaces()
	{
		return places;
	}

	/**
	 * @return ArrayList<Point>
	 */
	public ArrayList<Point> getAroundPlaces()
	{
		return aroundPlaces;
	}


}


