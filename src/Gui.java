import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;

public class Gui implements ActionListener
{
	private int size=5;
	private JFrame fr=new JFrame();
	private JPanel pa=new JPanel();
	private JPanel pButton=new JPanel();
	private JPanel pDirection=new JPanel();
	private JPanel pSizeShip=new JPanel();
	private JButton [] ship=new JButton[size];
	private JButton continueToGame=new JButton();
	private JButton bDW=new JButton();
	private JButton bDL=new JButton();
	private JButton bb2=new JButton();
	private JButton[][] arrayButton;
	private JLabel instruction=new JLabel();
	private JLabel instruction2=new JLabel();
	private JLabel eraserS=new JLabel();
	private JLabel eraserS2=new JLabel();
	private Image[]img;
	private Icon [] icon;
	private Board brd=new Board();
	private GuiGame gam;
	private String direction ="w";
	private int shipIndex =0;
	private int sizeOfShip=0;
	private int countClicked=0;
	private int [] shipSize={2,3,3,4,5};
	private int countShips=0;
	private boolean available=false;
	private boolean finish=false;
	private boolean fEraser=false;
	private boolean firstGame;


	/**
	 * @param firstG
	 * Constructor
	 */
	public Gui(boolean firstG)
	{
		firstGame=firstG;
		pa.setLayout(new BorderLayout());
		pButton.setLayout(new GridLayout(10,10));
		pSizeShip.setLayout(new GridLayout(5,1));
		img=new Image[8];
		icon=new Icon[8];
		//edit the button of the ships
		img[0]= pButton.getToolkit().getImage("Resources\\battleShip2.jpg");
		img[1]= pButton.getToolkit().getImage("Resources\\battleShip3.jpg");
		img[2]= pButton.getToolkit().getImage("Resources\\battleShip3.jpg");
		img[3]= pButton.getToolkit().getImage("Resources\\battleShip4.jpg");
		img[4]= pButton.getToolkit().getImage("Resources\\battleShip5.jpg");
		img[5]= pButton.getToolkit().getImage("Resources\\Length.png");
		img[6]= pButton.getToolkit().getImage("Resources\\Width.png");
		img[7]= pButton.getToolkit().getImage("Resources\\game.png");

		for(int i=0;i<ship.length;i++)
		{
			img[i]=img[i].getScaledInstance(100,100,Image.SCALE_DEFAULT);
			icon[i]=new ImageIcon(img[i]);
			ship[i]=new JButton();
			ship[i].addActionListener(this);
			ship[i].setPreferredSize(new Dimension(100,200));
			ship[i].setIcon(icon[i]);
			ship[i].setBackground(Color.white);
			pSizeShip.add(ship[i]);

		}
		img[5]=img[5].getScaledInstance(50,50,Image.SCALE_DEFAULT);
		icon[5]=new ImageIcon(img[5]);
		bDL.setIcon(icon[5]);
		bDL.setBackground(Color.BLACK);
		bDL.addActionListener(this);

		img[6]=img[6].getScaledInstance(50,50,Image.SCALE_DEFAULT);
		icon[6]=new ImageIcon(img[6]);
		bDW.setIcon(icon[6]);
		bDW.setBackground(Color.black);
		bDW.addActionListener(this);

		img[7]=img[7].getScaledInstance(150,100,Image.SCALE_DEFAULT);
		icon[7]=new ImageIcon(img[7]);
		continueToGame.setIcon(icon[7]);
		continueToGame.setBackground(Color.black);
		continueToGame.setSize(150,100);
		pDirection.add(continueToGame);
		continueToGame.addActionListener(this);
		continueToGame.setEnabled(false);
		instruction.setText("<html><br><br><br><br>בחר צוללת אחת<html>");
		instruction.setFont(new Font("arial",Font.BOLD,16));
		instruction.setForeground(Color.white);
		instruction2.setText("<html> מתוך חמשת הצוללות<html>");
		instruction2.setFont(new Font("arial",Font.BOLD,16));
		instruction2.setForeground(Color.white);

		eraserS.setText("<html><br><br><br><br> אם ברצונך למחוק צוללת- <br>");
		eraserS.setFont(new Font("arial",Font.BOLD,14));
		eraserS.setForeground(Color.red);
		eraserS2.setText("<html> לחץ עליה <br>");
		eraserS2.setFont(new Font("arial",Font.BOLD,14));
		eraserS2.setForeground(Color.red);


		arrayButton=new JButton[10][10];
		for(int i=0;i<arrayButton.length;i++)
			for(int j=0;j<arrayButton.length;j++)//do the button in the array
			{
				arrayButton[i][j]=new JButton();
				arrayButton[i][j].addActionListener(this);
				arrayButton[i][j].setPreferredSize(new Dimension(50,50));
				arrayButton[i][j].setBackground(Color.blue);
				arrayButton[i][j].setName(i+","+j);
				pButton.add(arrayButton[i][j]);

			}
		pDirection.add(bDW);
		pDirection.add(bDL);
		pDirection.add(instruction);
		pDirection.add(instruction2);
		pDirection.add(eraserS);
		pDirection.add(eraserS2);
		pDirection.setBackground(Color.black);
		pa.add(pButton,BorderLayout.EAST);
		pa.add(pSizeShip,BorderLayout.WEST);
		pa.add(pDirection,BorderLayout.CENTER);

		pa.setBackground(Color.black);
		fr.add(pa);
		fr.setSize(800,500);
		fr.setResizable(false);
		fr.setVisible(true);
	}

	/**
	 * @param e
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e)
	{

		boolean exception=false;
		JButton bb=(JButton)e.getSource();
		String name=bb.getName();
		boolean shipYesNo=false;
		if(bb==bDW)
		{
			direction ="w";
			instruction.setText("<html><br><br><br>לחץ על אחת מהמשבצות<br> <html>");
			instruction2.setText("<html>כדי להציב את הצוללת <html>");

		}
		else if(bb==bDL)
		{
			direction ="l";
			instruction.setText("<html><br><br><br>לחץ על אחת מהמשבצות<br> <html>");
			instruction2.setText("<html>כדי להציב את הצוללת <html>");

		}
		else if(bb!=bDL&&bb!=bDW&&bb!=ship[0]&&bb!=ship[1]&&bb!=ship[2]&&bb!=ship[3]&&bb!=ship[4]&&bb!=continueToGame)
		{
			instruction.setText("<html><br><br><br>לחץ על אחת מהמשבצות<br> <html>");
			instruction2.setText("<html>כדי להציב את הצוללת <html>");
			if(bb.getBackground().equals(Color.red))
			{
				if_remove_ship(bb);
			}
			Point p1=new Point (brd.from_string_to_point(bb.getName()));
			exception=brd.is_exception_board(p1, direction,sizeOfShip);
			countClicked=0;
			if (exception)
			{
				JOptionPane.showMessageDialog(null, "הסירה יוצאת מגבולות הלוח  ");
				ship[shipIndex].setEnabled(true);
				shipYesNo=false;
				sizeOfShip=0;
			}
			else
			{
				available =brd.checkIfAvailable(p1,sizeOfShip, direction);

				if (sizeOfShip==0&&countShips!=5)
				{
					JOptionPane.showMessageDialog(null, "עלייך לבחור סירה חדשה ");

				}
				else if (!available &&!finish)
				{
					JOptionPane.showMessageDialog(null, "אינך יכול למקם את הסירה במקום זה");

					sizeOfShip=0;
					ship[shipIndex].setEnabled(true);
					shipYesNo=false;
				}
				if(available)
				{
					brd.placed(p1, direction, shipIndex);
					countShips++;
				}
				sizeOfShip=0;

				for (int  i= 0;  i< arrayButton[0].length; i++)
				{
					for (int j = 0; j < arrayButton[1].length; j++)
					{
						if (brd.get_cell_num(new Point(i,j))== shipIndex&& brd.get_cell_num(new Point(i,j))!= -1)
							arrayButton[i][j].setBackground(Color.red);
					}
				}

			}
		}
		if(bb==ship[0]||bb==ship[1]||bb==ship[2]||bb==ship[3]||bb==ship[4])
		{
			instruction.setText("<html><br><br><br><br><br>בחר את כיוון הסירה<html>");
			instruction2.setText("");
			for(int i=0;i<ship.length && !shipYesNo;i++)
			{
				if(bb==ship[i])
				{
					ship[i].setEnabled(false);
					shipIndex =i;
					shipYesNo=false;
					sizeOfShip=shipSize[i];
				}
			}
			countClicked++;

			if(countClicked==1)
				bb2=bb;
			if(countClicked==2)
			{
				bb2.setEnabled(true);
				countClicked=1;
				bb2=bb;

			}
		}

		if(countShips==5&&bb!=continueToGame&&!finish)
		{
			sizeOfShip=0;
			finish=true;
			JOptionPane.showMessageDialog(null, "סיימת למקם את הסירות,אם ברצונך להמשיך למשחק לחץ על הכפתור המופיע בתחתית המסך  ");
			continueToGame.setEnabled(true);
		}


		if(bb==continueToGame)
		{
			fr.setVisible(false);
			if(firstGame)
			{
				gam=new GuiGame(this, firstGame);
				gam.set_board(brd);
			}
			else
			{
				gam.set_board(brd);
				gam.create_new_game();
			}
		}

	}


	/**
	 * @param b
	 * The method check if the user want to remove ship, and remove it.
	 */
	public void if_remove_ship(JButton b)
	{
		fEraser=false;
		int s;
		int result = JOptionPane.showConfirmDialog(null,"האם ברצונך להסיר את הסירה מהלוח? ",null, JOptionPane.YES_NO_OPTION);
		if(result == JOptionPane.YES_OPTION)
		{
			countShips--;
			finish=false;
			for(int i=0;i<arrayButton.length&&!fEraser;i++)
			{
				for(int j=0;j<arrayButton.length&&!fEraser;j++)
				{
					if(arrayButton[i][j]==b)
					{
						fEraser=true;
						s=brd.get_boardMat()[i][j].getIndex_ship();
						String dEraser=brd.getShips()[s].getDirection();
						for(int t=0;t<brd.getShips()[s].getPlaces().size();t++)
						{
							if(dEraser.equals("w"))
							{
								arrayButton[brd.getShips()[s].getPlaces().get(t).x][brd.getShips()[s].getPlaces().get(t).y].setBackground(Color.blue);
							}
							if(dEraser.equals("l"))
							{
								arrayButton[brd.getShips()[s].getPlaces().get(t).y][brd.getShips()[s].getPlaces().get(t).x].setBackground(Color.blue);
							}

						}
						continueToGame.setEnabled(false);
						ship[s].setEnabled(true);
						brd.remove_ship(s,dEraser);
					}
				}
			}
		}
		if(result==JOptionPane.NO_OPTION)
		{

		}
	}


	/**
	 * The method reset the game for the second game
	 */
	public void resetGame()
	{
		firstGame=false;
		for(int i = 0; i<brd.get_boardMat().length; i++)
		{
			for(int j = 0; j<brd.get_boardMat().length; j++)
			{
				brd.get_boardMat()[i][j].reset_cell();
				arrayButton[i][j].setBackground(Color.blue);
			}
		}
		for(int i=0;i<ship.length;i++)
		{
			ship[i].setEnabled(true);
			brd.getShips()[i].delete_around_ship();
			brd.getShips()[i].delete_ship_from_board();
		}
		continueToGame.setEnabled(false);
		countClicked=0;
		countShips=0;
		available =false;
		fEraser=false;
		finish=false;
		shipIndex =0;
		direction ="w";
		sizeOfShip=0;
		fr.setVisible(true);
	}
} 



